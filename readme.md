
# Химерні пригоди 2

Коротка пригодницька гра на Bitsy рушії. Ви граєте за прислужника Пальми 3000 і повинні змусити художницю Ромашку малювати. Зробити це вам доведеться за допомогою кинджалу, вудуїзму, невидимої руки ринку та чинного українського законодавства.

![Screenshot](./screenshot.gif)

![Screenshot2](./screenshot2.gif)

Керування грою відбувається з клавіатури стрілками.

**[Грати онлайн](https://palma3000.itch.io/4dv3ntur3v2)**

## License

```
           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
                   Version 2, December 2004

Copyright (C) 2024 Palma3000 <palma3000@airmail.cc>

Everyone is permitted to copy and distribute verbatim or modified
copies of this license document, and changing it is allowed as long
as the name is changed.

           DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
  TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

 0. You just DO WHAT THE FUCK YOU WANT TO.
```

